import numpy as np
import matplotlib.pyplot as plt

# Ex 4

A = [[0.8, 3.3, 0], [0, 1.5, 0], [1.7, 0, 0.5]]


def check_if_converge(matrix):
    lambdas = [0] * len(matrix)
    converges = 1
    for r in range(len(matrix)):
        for c in range(len(matrix)):
            if r == c:
                lambdas[r] = 1 - matrix[r][c]
                if abs(lambdas[r]) > 1:
                    print("Doesn't converge, lambda[", r + 1, "] > 1")
                    converges = 0
                    break
    if converges == 1:
        print("Converges")


# check_if_converge(A)

# Ex 5


def coef(x, y):
    n = len(x)
    a = []
    for i in range(n):
        a.append(y[i])

    for j in range(1, n):

        for i in range(n - 1, j - 1, -1):
            a[i] = float(a[i] - a[i - 1]) / float(x[i] - x[i - j])
    return np.array(a)  # повертає масив коефіцієнтів


def Eval(a, x, r):
    #    a : масив коефіцієнтів
    #    r : точка інтерполяції
    n = len(a) - 1
    temp = a[n] + (r - x[n])
    for i in range(n - 1, -1, -1):
        temp = temp * (r - x[i]) + a[i]
    return temp  # повертає наближене (інтерпольоване значення)




x = [1, 1.2, 1.4]
y = [7.389, 11.02, 16.44]
r = 1.3
coefs = coef(x, y)
value = Eval(coefs, x, r)

print(coefs)
print(value)

